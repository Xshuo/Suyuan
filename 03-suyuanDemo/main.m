//
//  main.m
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/9/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
