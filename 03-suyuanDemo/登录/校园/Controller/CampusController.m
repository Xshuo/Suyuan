//
//  CampusController.m
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/10/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "CampusController.h"
#import "KIZImageScrollView.h"
@interface CampusController ()<KIZImageScrollViewDelegate,KIZImageScrollViewDatasource>

@property (weak, nonatomic) IBOutlet KIZImageScrollView *imageScrollView;
@property (nonatomic,strong) NSArray *dataSourse;
@end

@implementation CampusController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSourse = @[@"testimage0",@"testimage1",@"new_feature_1"];
    self.imageScrollView.kizScrollDelegate = self;
    self.imageScrollView.kizScrollDataSource = self;
    
    [self performSelector:@selector(fetchData) withObject:nil afterDelay:2];
}

- (NSUInteger)numberOfImageInScrollView:(KIZImageScrollView *)scrollView {
    return self.dataSourse.count;
}

- (void)scrollView:(KIZImageScrollView *)scrollView imageAtIndex:(NSUInteger)index forImageView:(UIImageView *)imageView {
    imageView.image = [UIImage imageNamed:self.dataSourse[index]];
}

- (void)fetchData {
    self.dataSourse = @[@"testimage0",@"testimage1",@"new_feature_1"];
    [self.imageScrollView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
