//
//  LoginController.m
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/9/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "LoginController.h"
#import "MBProgressHUD.h"
#import "HttpClient.h"
#import "AFNetworking.h"
#import "Json.h"
#import "WsqMD5Util.h"
#import "PersonTableController.h"
#import "TabBarViewController.h"

@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loginBtn.layer.cornerRadius = 6;
    self.loginBtn.layer.masksToBounds = YES;
    self.registerBtn.layer.cornerRadius = 6;
    self.registerBtn.layer.masksToBounds = YES;
}


- (IBAction)loginBtn:(id)sender {
    
    //检查用户输入
    if ([self.usernameText.text isEqualToString:@""] || [self.usernameText.text isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码不能为空" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        return ;
    }
    
    NSDictionary *parameters = @{@"imme": @"Login",@"username":self.usernameText.text,@"passwd": [WsqMD5Util getmd5WithString:self.passwordText.text]};
    
    [[[HttpClient alloc] getAFHTTPRequestOperationManager] POST:[[Sysconfig alloc] getServerAddress:@"Api_Login"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *requestTmp = [NSString stringWithString:operation.responseString];
        
        // 获取数据成功  解析JSON数据
        Json *json = [[Json alloc] init];
        
        // 服务器信息判断
        if ([json Init:requestTmp]) {
            
            NSLog(@"初始化成功");
            
            // 用户登录信息判断
            if ([[json getrootValue:@"returncode"] isEqualToString:@"0"]) {
                
                NSLog(@"%@",[json getrootValue:@"message"]);
                Sysconfig *sysconfig = [[Sysconfig alloc] init];
                
                // 切换到userinfo数组
                [json getJsonArry:@"userinfo"];
                NSString *userPassword = [json getArrayValue:0 colname:@"passwd"];
                NSString *birthday = [json getArrayValue:0 colname:@"birthday"];
                NSString *nickname = [json getArrayValue:0 colname:@"nickname"];
                NSString *userid = [json getArrayValue:0 colname:@"ID"];
                NSString *phone = [json getArrayValue:0 colname:@"phone"];
                NSString *stunum = [json getArrayValue:0 colname:@"stunum"];
                NSString *sex = [json getArrayValue:0 colname:@"sex"];
                NSString *picurl = [json getArrayValue:0 colname:@"picurl"];
                NSString *country = [json getArrayValue:0 colname:@"country"];
            
                
                [sysconfig setUserDataWithKey:@"password" WithVaule:userPassword];
                [sysconfig setUserDataWithKey:@"birthday" WithVaule:birthday];
                [sysconfig setUserDataWithKey:@"nickname" WithVaule:nickname];
                [sysconfig setUserDataWithKey:@"userid" WithVaule:userid];
                [sysconfig setUserDataWithKey:@"phone" WithVaule:phone];
                [sysconfig setUserDataWithKey:@"stunum" WithVaule:stunum];
                [sysconfig setUserDataWithKey:@"sex" WithVaule:sex];
                [sysconfig setUserDataWithKey:@"nickname" WithVaule:nickname];
                [sysconfig setUserDataWithKey:@"picurl" WithVaule:picurl];
                [sysconfig setUserDataWithKey:@"country" WithVaule:country];
                
                // 切换到token数组
                [json getJsonArry:@"token"];
                NSString *token = [json getArrayValue:0 colname:@"token"];
                
                [sysconfig setUserDataWithKey:@"token" WithVaule:token];
                
                [self showAlertView];
                // 登录成功 跳转页面
                TabBarViewController *tabBarVC = [[TabBarViewController alloc] init];
                [self presentViewController:tabBarVC animated:YES completion:nil];
                
            }else {
                
                NSLog(@"%@",[json getrootValue:@"message"]);
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                [alertView show];
                return ;
            }
            
        }else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络回家吃饭了，稍等" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alertView show];
            return ;
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    }];
    
}

- (void)showAlertView {
    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hub.labelText = @"正在登录...";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//触摸其他位置隐藏键盘
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.usernameText resignFirstResponder];
    [self.passwordText resignFirstResponder];
}


@end
