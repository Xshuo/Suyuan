//
//  UI.m
//  华祥
//
//  Created by dev on 14/12/4.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import "UI.h"
#import "MBProgressHUD.h"

@implementation UI


-(void) Toast:(NSString *)mess :(UIView *)view
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = mess;
    hud.margin = 15.f;
    CGRect rect = [[UIScreen mainScreen] bounds];
    hud.yOffset = rect.size.height/3;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:2];
}

@end
