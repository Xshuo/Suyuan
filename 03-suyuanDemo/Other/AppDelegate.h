//
//  AppDelegate.h
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/9/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder<UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
