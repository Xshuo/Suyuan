//
//  ModifySexController.m
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/10/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "ModifySexController.h"

@interface ModifySexController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *titleLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ModifySexController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedRow inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
}

- (IBAction)cancelBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *ident = @"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:ident];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"男";
    }else{
        cell.textLabel.text = @"女";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    
    self.selectedRow = indexPath.row;
    [self.tableView reloadData];
    if (self.selectedRow == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveSex" object:@"男" userInfo:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveSex" object:@"女" userInfo:nil];
    }
}

- (UITableViewCellAccessoryType)tableView:(UITableView*)tableView accessoryTypeForRowWithIndexPath:(NSIndexPath*)indexPath
{
    if(self.selectedRow == indexPath.row)
    {
        return UITableViewCellAccessoryCheckmark;
    }
    else
    {
        return UITableViewCellAccessoryNone;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
