//
//  TestController.m
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/10/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "TestController.h"
#import "UserInfoController.h"
@interface TestController ()

@property (weak, nonatomic) IBOutlet UITextField *nicknameText;

@end

@implementation TestController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"name=%@",self.name);
    self.nicknameText.text = self.name;
    
}
- (IBAction)cancelBtn:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
