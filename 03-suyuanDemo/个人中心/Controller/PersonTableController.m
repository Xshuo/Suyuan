//
//  PersonTableController.m
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/9/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "PersonTableController.h"
#import "LoginController.h"
#import "HeadCell.h"
#import "SettingCell.h"

@interface PersonTableController ()

@property (nonatomic, strong) NSArray *settingImage;
@property (nonatomic, strong) NSArray *settingText;
@property (nonatomic, strong) LoginController *loginVC;
@end

@implementation PersonTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.settingImage = [NSArray arrayWithObjects:@"photo",@"collect",@"wallet",@"face",@"setting",nil];
    self.settingText = [NSArray arrayWithObjects:@"相册",@"收藏",@"钱包",@"表情",@"设置", nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 4;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        HeadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeadCell" forIndexPath:indexPath];
        cell.headImage.layer.cornerRadius = 10;
        cell.clipsToBounds = YES;
        return cell;
    }else{
        SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
        if (indexPath.section == 1) {
            cell.iconImage.image = [UIImage imageNamed:self.settingImage[indexPath.row]];
            cell.settingText.text = self.settingText[indexPath.row];
            return cell;
        }else if (indexPath.section == 2) {
            cell.iconImage.image = [UIImage imageNamed:@"face"];
            cell.settingText.text = @"表情";
            return cell;
        }else if (indexPath.section == 3) {
            cell.iconImage.image = [UIImage imageNamed:@"setting"];
            cell.settingText.text = @"设置";
            return cell;
            
        }else {
            return cell;
        }
        
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 22;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 90;
    }else {
        return 50;
    }
}


@end
