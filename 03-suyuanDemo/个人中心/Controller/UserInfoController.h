//
//  UserInfoController.h
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/9/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserInfoControllerDelegate <NSObject>

- (void)modifyHeadImage:(UIImage *)headImage;

@end

@interface UserInfoController : UITableViewController

@property (nonatomic, weak) id <UserInfoControllerDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLaebl;

@end
