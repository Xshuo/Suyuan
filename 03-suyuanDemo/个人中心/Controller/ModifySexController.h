//
//  ModifySexController.h
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/10/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModifySexController : UIViewController

@property (nonatomic, strong) NSString *sex;

@property (nonatomic, assign) NSInteger selectedRow;

@end
