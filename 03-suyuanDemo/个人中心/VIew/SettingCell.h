//
//  SettingCell.h
//  suyuanDemo
//
//  Created by 徐宜硕 on 15/9/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *settingText;


@end
