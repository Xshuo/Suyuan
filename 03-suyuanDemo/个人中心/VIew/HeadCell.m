//
//  HeadCell.m
//  suyuanDemo//
//  Created by 徐宜硕 on 15/9/22.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "HeadCell.h"
#import "Json.h"
#import "Sysconfig.h"
@implementation HeadCell

- (void)awakeFromNib {
    // 读取本地的nickname数据 并赋值到控制器中
    NSUserDefaults *nicknameDefualts = [NSUserDefaults standardUserDefaults];
    NSString *nickname = [nicknameDefualts objectForKey:@"nickname"];
    self.nickNameLabel.text = nickname;
    
    // 读取本地的图片地址数据 并赋值到控制器中
    NSUserDefaults *headImageDefualts = [NSUserDefaults standardUserDefaults];
    NSString *picUrlStr = [headImageDefualts objectForKey:@"picurl"];
    NSString *picHttp = [NSString stringWithFormat:@"%@%@",top,picUrlStr];
    NSURL *picurl = [NSURL URLWithString:picHttp];
    NSData *data = [NSData dataWithContentsOfURL:picurl];
    self.headImage.layer.cornerRadius = 8;
    self.headImage.clipsToBounds = YES;
    self.headImage.image = [UIImage imageWithData:data];
    
    // 添加头像边框效果
    self.headImage.layer.borderColor = [UIColor grayColor].CGColor;
    self.headImage.layer.borderWidth = 0.5;
    

    
    NSUserDefaults *sexDefualts = [NSUserDefaults standardUserDefaults];
    NSString *sex = [sexDefualts objectForKey:@"sex"];
    self.sexLabel.text = sex;

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
