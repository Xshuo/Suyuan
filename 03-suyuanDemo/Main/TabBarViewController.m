//
//  TabBarViewController.m
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/9/23.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "TabBarViewController.h"

#import "DiscoverController.h"
#import "CampusController.h"
#import "MessageController.h"
#import "PersonTableController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /**
     *  添加控制器
     */
    
    // 校园界面
    UIStoryboard *campusStoryborad = [UIStoryboard storyboardWithName:@"Campus" bundle:nil];
    
    CampusController *campusVC = [campusStoryborad instantiateViewControllerWithIdentifier:@"CampusVC"];
    
    [self addViewController:campusVC WithTitle:@"校园" WithImage:@"tabbar_home" WithSelectImage:@"tabbar_home_selected"];
    
    
    DiscoverController *discoverVC = [[DiscoverController alloc] init];
    [self addViewController:discoverVC WithTitle:@"发现" WithImage:@"tabbar_discover" WithSelectImage:@"tabbar_discover_selected"];
    
    MessageController *messageVC = [[MessageController alloc] init];
    [self addViewController:messageVC WithTitle:@"泡泡" WithImage:@"tabbar_message_center" WithSelectImage:@"tabbar_message_center_selected"];
    
    //个人中心界面
    UIStoryboard *personStoryboard = [UIStoryboard storyboardWithName:@"Person" bundle:nil];
    
    PersonTableController *personVC = [personStoryboard instantiateViewControllerWithIdentifier:@"PersonVC"];
    
    [self addViewController:personVC WithTitle:@"个人" WithImage:@"tabbar_profile" WithSelectImage:@"tabbar_profile_selected"];

}

- (void)addViewController:(UIViewController *)viewCintroller WithTitle:(NSString *)title WithImage:(NSString *)imageName WithSelectImage:(NSString *)selectImage {
    
    viewCintroller.title = title;
    
    viewCintroller.tabBarItem.image = [UIImage imageNamed:imageName];
    
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
    //NSAttributedString.h
    mutDic[NSForegroundColorAttributeName] = [UIColor orangeColor];
    [viewCintroller.tabBarItem setTitleTextAttributes:mutDic forState:UIControlStateSelected];
    
    //取消系统控制器的渲染效果
    UIImage *selectTemoImage = [[UIImage imageNamed:selectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    viewCintroller.tabBarItem.selectedImage = selectTemoImage;
    
    viewCintroller.view.backgroundColor = [UIColor colorWithRed:(239)/255.0 green:(239)/255.0 blue:(244)/255.0 alpha:1.0];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewCintroller];
    
    [self addChildViewController:nav];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
