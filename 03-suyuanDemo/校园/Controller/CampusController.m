//
//  CampusController.m
//  03-suyuanDemo
//
//  Created by 徐宜硕 on 15/10/13.
//  Copyright © 2015年 Xshuo. All rights reserved.
//

#import "CampusController.h"

@interface CampusController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageContro;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSTimer *timer;
@end

#define imageCount 2
@implementation CampusController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 屏幕宽高
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    // 图片的宽
    CGFloat imageW = screenW;
    // 图片的高
    CGFloat imageH = self.scrollView.frame.size.height;
    // 图片的Y
    CGFloat imageY = 0;
    
    // for循环添加图片
    for (int i = 0; i<imageCount; i++) {
        // 图片的X
        CGFloat imageX = i * imageW;
        // 设置frame
        self.imageView.frame = CGRectMake(imageX, imageY, imageW, imageH);
        // 设置图片
        NSString *imageName = [NSString stringWithFormat:@"testimage%d",i];
        self.imageView.image = [UIImage imageNamed:imageName];
        // 将imageView 添加到 scrollView
        [self.scrollView addSubview:self.imageView];
    }
    // 设置scrollView的滚动范围
    CGFloat contentW = imageCount * imageW;
    // 不允许在垂直方向是那个进行滚动
    self.scrollView.contentSize = CGSizeMake(contentW, 0);
    // 设置分页
    self.scrollView.pagingEnabled = YES;
    // 设置分页的个数
    self.pageContro.numberOfPages = imageCount;
    // 监听scrollView的滚动
    self.scrollView.delegate = self;
    
    [self addTimerToScroll];
    
}

- (void)viewDidAppear:(BOOL)animated {
//    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
//    NSLog(@"%f",screenW);
}
#pragma mark 初始化定时器
- (void)addTimerToScroll {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
}

#pragma mark 去除定时器
- (void)removeTimerToScroll {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark 跳转到下一张图片
- (void) nextPage {
    
    NSInteger page = self.pageContro.currentPage;
    if (page == imageCount - 1) {
        page = 0;
        [self.scrollView setContentOffset:CGPointZero animated:YES];
    }else {
        page ++;
        [self.scrollView setContentOffset:CGPointMake(page * self.scrollView.frame.size.width, 0) animated:YES];
    }
    
}

#pragma mark 开始抓动时 停止定时器
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    NSLog(@"beginDragging");
    if (self.scrollView == scrollView) {
        [self removeTimerToScroll];
    }
}

#pragma mark 结束抓动时  开始定时器
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    NSLog(@"DidEndDragging");
    
    if (self.scrollView == scrollView) {
        [self addTimerToScroll];
    }
}

#pragma mark scrollView的滚动设置
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGFloat offSetX = scrollView.contentOffset.x;
    
    self.pageContro.currentPage = offSetX/self.scrollView.frame.size.width;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
