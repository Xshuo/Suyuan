//
//  API.m
//  华祥
//
//  Created by dev on 14/11/24.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import "HttpClient.h"
#import "AFNetworking.h"
#import "Sysconfig.h"


@implementation HttpClient

-(AFHTTPRequestOperationManager *)getAFHTTPRequestOperationManager{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    return manager;
}

@end
