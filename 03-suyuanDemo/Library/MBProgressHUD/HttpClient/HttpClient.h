//
//  API.h
//  华祥
//
//  Created by dev on 14/11/24.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Sysconfig.h"

@interface HttpClient : NSObject

//获取地址映射接口
-(AFHTTPRequestOperationManager *)getAFHTTPRequestOperationManager;

@end
