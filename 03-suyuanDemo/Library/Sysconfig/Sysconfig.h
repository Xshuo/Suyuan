//
//  Sysconfig.h
//  华祥
//
//  Created by dev on 14/11/24.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sysconfig : NSObject
{
}

 
//获取服务器api地址
-(NSString *)getServerAddress:(NSString *)apiname;

//获取用户数据
-(NSString *)getUserDataWithKey:(NSString *)key ;


//设置用户名数据
-(void)setUserDataWithKey:(NSString *)key WithVaule:(NSString *)value;




@end
