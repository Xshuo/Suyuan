//
//  Sysconfig.m
//  华祥
//
//  Created by dev on 14/11/24.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import "Sysconfig.h"

@implementation Sysconfig

//获取服务器api地址
-(NSString *)getServerAddress:(NSString *)apiname
{
    return [NSString stringWithFormat:@"%@%@",@"http://crater.top:6002/",apiname];
}

- (NSString *)getUserDataWithKey:(NSString *)key {
    //获取UserDefault
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *username = [userDefault objectForKey:key];
    return username;
}

- (void)setUserDataWithKey:(NSString *)key WithVaule:(NSString *)value {
    
    //获取userDefault单例
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //设置键值对
    [userDefaults setObject:value forKey:key];
    
}


@end
