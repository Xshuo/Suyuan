//
//  Json.h
//  华祥
//
//  Created by dev on 14/11/25.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Json : NSObject
{
    @public
    NSData *data;
    NSDictionary *resultDic;
    NSArray *resultArr;
    long arraycount;
}

//获取json根中的值
-(NSString *)getrootValue:(NSString *)colname;

//获取json数组中的值
-(NSString *)getArrayValue:(int)rowsindex colname:(NSString *)colname;

//初始化json数组
-(BOOL)getJsonArry:(NSString *)jsonArrayName;

//初始化json类
-(BOOL)Init:(NSString *)jsonString;

@end
