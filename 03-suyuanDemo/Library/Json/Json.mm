//
//  Json.m
//  华祥
//
//  Created by dev on 14/11/25.
//  Copyright (c) 2014年 smart. All rights reserved.
//

#import "Json.h"

@implementation Json

//初始化json类
-(BOOL)Init:(NSString *)jsonString
{
    data = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//    NSLog(@"%@",jsonString);
    if (resultDic == nil) {
        return false;
    }else {
        return true;
    }
}

//初始化json数组
-(BOOL)getJsonArry:(NSString *)jsonArrayName
{
    @try {
        resultArr = [resultDic objectForKey:jsonArrayName];
        arraycount = [resultArr count];
        return true;
    }
    @catch (NSException *exception) {
        return false;
    }
}

//获取json数组中的值
-(NSString *)getArrayValue:(int)rowsindex colname:(NSString *)colname
{
    @try {
        NSDictionary *value = [resultArr objectAtIndex:rowsindex];
        return [value objectForKey:colname];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

//获取json根中的值
-(NSString *)getrootValue:(NSString *)colname
{
    @try {
        return [resultDic objectForKey:colname];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

@end
